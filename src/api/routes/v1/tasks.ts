import express from "express";
import { Todo } from "interfaces";

const router = express.Router();

const taskMocks: Todo[] = Array.from({ length: 15 }, (_, i) => i).map((i) => ({
  id: i + 1,
  userId: Math.floor(i / 5) + 1,
  title: `Task${i + 1}`,
  content: `This is Task${i + 1}.`,
  isChecked: false,
}));

router.get("/", (req: express.Request, res: express.Response) => {
  try {
    const queryUserIds: number[] = req.query.userId
      ? req.query.userId
          .toString()
          .split(",")
          .map((id) => parseInt(id))
      : [];
    const taskData = !queryUserIds.length
      ? taskMocks
      : taskMocks.filter((task) => queryUserIds.includes(task.userId));
    res.status(200).json({ tasks: taskData });
  } catch (err) {
    if (err instanceof Error) {
      res.status(400).json({ message: err.message });
    }
    res.status(400).json({ message: "Failed: something wrong was happened." });
  }
});

export default router;
