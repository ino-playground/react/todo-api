import express, {
  Request as ExRequest,
  Response as ExResponse,
  NextFunction,
} from "express";
import router from "./api/routes/v1/";

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use((req: ExRequest, res: ExResponse, next: NextFunction) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Credentials", "true");

  if ("OPTIONS" == req.method) {
    res.send(204); // 204: No Content
  } else {
    next();
  }
});

app.use("/api/v1", router);
app.use(
  (
    err: unknown,
    req: ExRequest,
    res: ExResponse,
    next: NextFunction
  ): ExResponse | void => {
    if (err instanceof Error) {
      console.warn(`Caught Validation Error for ${req.path}:`, err.message);

      return res.status(422).json({
        message: "Validation Failed",
        details: err.message,
      });
    }
    if (err instanceof Error) {
      return res.status(500).json({
        message: "Internal Server Error",
      });
    }

    next();
  }
);

const port = process.env.PORT || 8080;

app.listen(port);
console.log(`Express WebAPI listening on port ${port}`);
